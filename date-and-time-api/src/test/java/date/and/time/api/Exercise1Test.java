package date.and.time.api;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;

import org.junit.Test;

import common.test.tool.annotation.Necessity;
import common.test.tool.dataset.DateAndTimes;
import date.and.time.api.exercice1.LocalDateIsAfterExercise;
import date.and.time.api.exercice1.LocalDateMinusExercise;
import date.and.time.api.exercice1.LocalDateOfExercise;
import date.and.time.api.exercice1.LocalDateParseExercise;
import date.and.time.api.exercice1.LocalDatePlusExercise;
import date.and.time.api.exercice1.LocalDatePlusPeriodExercise;
import date.and.time.api.exercice1.LocalDateUntilExercise;
import date.and.time.api.exercice1.LocalDateWithAdjusterExercise;
import date.and.time.api.exercice1.LocalDateWithExercise;

public class Exercise1Test {

    @Test
    @Necessity(true)
    public void localDateOf() {

        LocalDate localDate = new LocalDateOfExercise().localDateOf();

        assertNotNull("You must return a LocalDate", localDate);
        assertThat("The date is not correct", localDate.toString(), is("2015-06-18"));
    }

    @Test
    @Necessity(true)
    public void localDateParse() {

        LocalDate localDate = new LocalDateParseExercise().localDateParse();

        assertNotNull("You must return a LocalDate", localDate);
        assertThat("The date is not correct", localDate.toString(), is("2015-06-18"));
    }

    @Test
    @Necessity(true)
    public void localDateWith() {
        LocalDate ld = DateAndTimes.LD_20150618;

        LocalDate localDate = new LocalDateWithExercise().localDateWith(ld);

        assertNotNull("You must return a LocalDate", localDate);
        assertThat("Years are not correct", localDate.getYear(), is(2016));
        assertThat("Months are not correct", localDate.getMonth(), is(ld.getMonth()));
        assertThat("Days are not correct", localDate.getDayOfMonth(), is(ld.getDayOfMonth()));
    }

    @Test
    @Necessity(true)
    public void localDateWithAdjuster() {
        LocalDate ld = DateAndTimes.LD_20150618;

        LocalDate localDate = new LocalDateWithAdjusterExercise().localDateWithAdjuster(ld);

        assertNotNull("You must return a LocalDate", localDate);
        assertThat("Years are not correct", localDate.getYear(), is(ld.getYear() + 1));
        assertThat("Months are not correct", localDate.getMonth(), is(Month.JANUARY));
        assertThat("Days are not correct", localDate.getDayOfMonth(), is(1));
    }

    @Necessity(true)
    @Test
    public void localDatePlus() {
        LocalDate ld = DateAndTimes.LD_20150618;

        LocalDate localDate = new LocalDatePlusExercise().localDatePlus(ld);

        assertNotNull("You must return a LocalDate", localDate);
        assertThat("Years are not correct", localDate.getYear(), is(ld.getYear() + 1));
        assertThat("Months are not correct", localDate.getMonth(), is(Month.APRIL));
        assertThat("Days are not correct", localDate.getDayOfMonth(), is(ld.getDayOfMonth()));
    }

    @Test
    @Necessity(true)
    public void localDateMinus() {
        LocalDate ld = DateAndTimes.LD_20150618;

        LocalDate localDate = new LocalDateMinusExercise().localDateMinus(ld);

        assertNotNull("You must return a LocalDate", localDate);
        assertThat("Years are not correct", localDate.getYear(), is(ld.getYear()));
        assertThat("Months are not correct", localDate.getMonth(), is(ld.getMonth()));
        assertThat("Days are not correct", localDate.getDayOfMonth(), is(ld.getDayOfMonth() - 10));
    }


    @Test
    @Necessity(true)
    public void localDatePlusPeriod() {
        LocalDate ld = DateAndTimes.LD_20150618;

        LocalDatePlusPeriodExercise exercise = new LocalDatePlusPeriodExercise();
        Period period = exercise.createPeriod();
        
        assertNotNull("You must return a Period", period);
        assertThat("Months are not correct", period.getMonths(), is(2));
        
        LocalDate localDate = exercise.localDatePlusPeriod(period, ld);

        assertNotNull("You must return a LocalDate", localDate);
        assertThat("Years are not correct", localDate.getYear(), is(ld.getYear() + 1));
        assertThat("Days are not correct", localDate.getDayOfMonth(), is(ld.getDayOfMonth() + 3));
    }

    @Test
    @Necessity(true)
    public void localDateIsAfter() {

        LocalDateIsAfterExercise exercise = new LocalDateIsAfterExercise();
        
        assertThat("ld2 is after ld", exercise.localDateIsAfter(DateAndTimes.LD_20150618, DateAndTimes.LD_20150807), is(true));
        assertThat("ld2 is not after ld", exercise.localDateIsAfter(DateAndTimes.LD_20150807, DateAndTimes.LD_20150618), is(false));
    }

    @Test
    @Necessity(true)
    public void localDateUntil() {
        LocalDate ld = DateAndTimes.LD_20150618;
        LocalDate ld2 = DateAndTimes.LD_20150807;

        Period period = new LocalDateUntilExercise().localDateUntil(ld, ld2);
        
        assertNotNull("You must return a Period", period);
        assertThat("Years are not correct", period.getYears(), is(0));
        assertThat("Months", period.getMonths(), is(1));
        assertThat("Days are not correct", period.getDays(), is(20));
    }
}

package date.and.time.api;

import common.test.tool.annotation.Necessity;
import common.test.tool.dataset.DateAndTimes;
import date.and.time.api.exercice2.LocalTimeIsBeforeExercise;
import date.and.time.api.exercice2.LocalTimeMinusDurationExercise;
import date.and.time.api.exercice2.LocalTimeMinusExercise;
import date.and.time.api.exercice2.LocalTimeOfHourToMinuteExercise;
import date.and.time.api.exercice2.LocalTimeOfHourToNanoSecExercise;
import date.and.time.api.exercice2.LocalTimeParseExercise;
import date.and.time.api.exercice2.LocalTimePlusExercise;
import date.and.time.api.exercice2.LocalTimeTruncatedToExercise;
import date.and.time.api.exercice2.LocalTimeWithExercise;

import org.junit.Test;

import java.time.Duration;
import java.time.LocalTime;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class Exercise2Test {

    @Test
    @Necessity(true)
    public void localTimeOfHourToMinute() {
        LocalTime localTime = new LocalTimeOfHourToMinuteExercise().localTimeOfHourToMinute();

        assertNotNull(localTime);
        assertThat(localTime.toString(), is("23:07"));
    }

    @Test
    @Necessity(true)
    public void localTimeOfHourToNanoSec() {
        LocalTime localTime = new LocalTimeOfHourToNanoSecExercise().localTimeOfHourToNanoSec();

        assertThat(localTime.toString(), is("23:07:03.100"));
    }

    @Test
    @Necessity(true)
    public void localTimeParse() {
        LocalTime localTime = new LocalTimeParseExercise().localTimeOfHourToNanoSec();

        assertNotNull(localTime);
        assertThat(localTime.toString(), is("23:07:03.100"));
    }

    @Test
    @Necessity(true)
    public void localTimeWith() {
        LocalTime lt = DateAndTimes.LT_23073050;

        LocalTime localTime = new LocalTimeWithExercise().localTimeWith(lt);

        assertNotNull(localTime);
        assertThat(localTime.getHour(), is(21));
        assertThat(localTime.getMinute(), is(lt.getMinute()));
        assertThat(localTime.getSecond(), is(lt.getSecond()));
    }

    @Test
    @Necessity(true)
    public void localTimePlus() {
        LocalTime lt = DateAndTimes.LT_23073050;

        LocalTime localTime = new LocalTimePlusExercise().localTimePlus(lt);

        assertNotNull(localTime);
        assertThat(localTime.getHour(), is(lt.getHour()));
        assertThat(localTime.getMinute(), is(lt.getMinute() + 30));
        assertThat(localTime.getSecond(), is(lt.getSecond()));
    }

    @Test
    @Necessity(true)
    public void localTimeMinus() {
        LocalTime lt = DateAndTimes.LT_23073050;

        LocalTime localTime = new LocalTimeMinusExercise().localTimeMinus(lt);

        assertNotNull(localTime);
        assertThat(localTime.getHour(), is(lt.getHour() - 3));
        assertThat(localTime.getMinute(), is(lt.getMinute()));
        assertThat(localTime.getSecond(), is(lt.getSecond()));
    }


    @Test
    @Necessity(true)
    public void localTimeMinusDuration() {
        LocalTime lt = DateAndTimes.LT_23073050;

        LocalTimeMinusDurationExercise exercise = new LocalTimeMinusDurationExercise();
        
        Duration duration = exercise.createDuration();
        
        assertNotNull(duration);
        assertThat(duration.getSeconds(), is(12620L));
        assertThat(duration.getNano(), is(200000000));
        
        LocalTime localTime = exercise.localTimeMinusDuration(duration, lt);

        assertNotNull(localTime);
        assertThat(localTime.getHour(), is(19));
        assertThat(localTime.getMinute(), is(37));
        assertThat(localTime.getSecond(), is(10));
        assertThat(localTime.getNano(), is(300000000));


    }

    @Test
    @Necessity(true)
    public void localDateIsBefore() {
        LocalTimeIsBeforeExercise exercise = new LocalTimeIsBeforeExercise();

        assertThat(exercise.localDateIsBefore(DateAndTimes.LT_23073050, DateAndTimes.LT_12100000), is(true));
        assertThat(exercise.localDateIsBefore(DateAndTimes.LT_12100000, DateAndTimes.LT_23073050), is(false));
    }

    @Test
    @Necessity(true)
    public void localTimeTruncatedTo() {
        LocalTime lt = DateAndTimes.LT_23073050;

        LocalTime localTime = new LocalTimeTruncatedToExercise().localTimeTruncatedTo(lt);

        assertNotNull(localTime);
        assertThat(lt.toString(), is("23:07:30.500"));
        assertThat(localTime.toString(), is("23:07"));
    }
}

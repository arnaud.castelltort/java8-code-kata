package date.and.time.api.exercice2;

import java.time.Duration;
import java.time.LocalTime;

public class LocalTimeMinusDurationExercise {
    /**
     * Define a {@link Duration} of 3 hours 30 minutes and 20.2 seconds
     */
    public Duration createDuration() {
        return null;
    }
    
    /**
     * Create a {@link LocalTime} subtracting the duration from {@link lt} by using {@link LocalTime#minus}
     */
    public LocalTime localTimeMinusDuration(Duration d, LocalTime lt) {
        return null;
    }
}

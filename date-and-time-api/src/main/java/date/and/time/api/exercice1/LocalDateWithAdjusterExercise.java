package date.and.time.api.exercice1;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

public class LocalDateWithAdjusterExercise {
    /**
     * Create a {@link LocalDate} from {@link ld} adjusted into first day of next year
     * by using {@link LocalDate#with} and {@link TemporalAdjusters#firstDayOfNextYear}
     */
    public LocalDate localDateWithAdjuster(LocalDate ld) {
        return null;
    }
}

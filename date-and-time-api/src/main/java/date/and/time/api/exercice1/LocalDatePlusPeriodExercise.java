package date.and.time.api.exercice1;

import java.time.LocalDate;
import java.time.Period;

public class LocalDatePlusPeriodExercise {
    /**
     * Define a {@link Period} of 1 year 2 month 3 days
     */
    public Period createPeriod() {
        return null;
    }
    
    /**
     * Create a {@link LocalDate} adding the period to {@link ld} by using {@link LocalDate#plus}
     */
    public LocalDate localDatePlusPeriod(Period p, LocalDate ld) {
        return null;
    }
}

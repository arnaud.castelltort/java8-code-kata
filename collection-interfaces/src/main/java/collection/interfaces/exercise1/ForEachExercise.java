package collection.interfaces.exercise1;

//[imports] { autofold
import common.test.tool.entity.Customer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
//}

public class ForEachExercise {
    /**
     * Create a {@link Consumer} which represents an operation to add customer's name to {@code nameList} list.
     * Iterate {@code customerIterable} with {@link Iterable#forEach} and use the {@link Consumer}
     * to finish creating the name list.
     */
    public List<String> iterateByForEach(Iterable<Customer> customerIterable) {
        return new ArrayList<>();
    }
}

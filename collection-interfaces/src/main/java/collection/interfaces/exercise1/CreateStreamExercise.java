package collection.interfaces.exercise1;

//[imports] { autofold
import java.util.Collection;
import java.util.stream.Stream;
//}

public class CreateStreamExercise {

    /**
     * Create a serial {@link Stream} using {@link Collection#stream}
     * You can learn about {@link Stream} APIs at stream-api module.
     */
    public Stream<String> createStream(Collection<String> nameList) {
        return null;
    }
}

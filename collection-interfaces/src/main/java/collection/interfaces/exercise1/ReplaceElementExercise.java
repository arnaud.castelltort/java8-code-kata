package collection.interfaces.exercise1;

//[imports] { autofold
import java.util.List;
import java.util.function.UnaryOperator;
//}

public class ReplaceElementExercise {
    /**
     * Create a {@link UnaryOperator} which returns given string wrapped with "()".
     * Replace the elements in {@link nameList} with string wrapped with "()" using {@link List#replaceAll} .
     */
    public void replaceTheElements(List<String> nameList) {
    }
}

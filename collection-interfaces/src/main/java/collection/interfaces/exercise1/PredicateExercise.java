package collection.interfaces.exercise1;

//[imports] { autofold
import java.util.Collection;
import java.util.function.Predicate;
//}

public class PredicateExercise {
    /**
     * Create a {@link Predicate} which predicates if the input string contains "e".
     * Remove elements from {@link nameCollection} which contains "e" using {@link Collection#removeIf}.
     */
    public void whoHaveNoEInYourName(Collection<String> nameCollection) {
    }
}

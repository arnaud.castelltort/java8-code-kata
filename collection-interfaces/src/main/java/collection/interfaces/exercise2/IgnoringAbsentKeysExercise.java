package collection.interfaces.exercise2;

//[imports] { autofold
import java.util.Map;
import java.util.function.BiFunction;
//}

public class IgnoringAbsentKeysExercise {
    /**
     * Try to increment the value for keys "Joe", "Steven" and "Alice" using {@link Map#computeIfPresent}.
     */
    public void ignoringAbsentKeys(Map<String, Integer> map) {
    }
}

package collection.interfaces.exercise2;

//[imports] { autofold
import java.util.Map;
import java.util.function.BiFunction;
//}

public class MergeValuesExercise {

    /**
     * Merge 2 entry to {@link map} with key="Alice" value=32, key="Joe" value=32 using {@link Map#merge}.
     * If the value already exist for the key, remap with sum value.
     */
    public void mergeValues(Map<String, Integer> map) {
    }
}

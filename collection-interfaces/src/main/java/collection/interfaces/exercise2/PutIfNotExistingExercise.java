package collection.interfaces.exercise2;

//[imports] { autofold
import java.util.Map;
//}

public class PutIfNotExistingExercise {
    /**
     * Try to put 2 entry with key as "Alice" value as 32, key as "Joe" and value as 32 using {@link Map#putIfAbsent}.
     */
    public void putIfNotExisting(Map<String, Integer> map) {
    }
}

package collection.interfaces;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Test;

import collection.interfaces.exercise1.CreateParallelStreamExercise;
import collection.interfaces.exercise1.CreateStreamExercise;
import collection.interfaces.exercise1.ForEachExercise;
import collection.interfaces.exercise1.PredicateExercise;
import collection.interfaces.exercise1.ReplaceElementExercise;
import collection.interfaces.exercise1.SortExercise;
import common.test.tool.annotation.Necessity;
import common.test.tool.dataset.ClassicOnlineStore;
import common.test.tool.entity.Customer;

public class Exercise1Test extends ClassicOnlineStore {

    @Test
    @Necessity(true)
    public void iterateByForEach() {
        Iterable<Customer> customerIterable = this.mall.getCustomerList();

        List<String> nameList = new ForEachExercise().iterateByForEach(customerIterable);

        assertThat("The result list is not correct", nameList.toString(), is("[Joe, Steven, Patrick, Diana, Chris, Kathy, Alice, Andrew, Martin, Amy]"));
    }

    @Test
    @Necessity(true)
    public void whoHaveNoEInYourName() {
        Collection<String> nameCollection = new ArrayList<>(Arrays.asList("Joe", "Steven", "Patrick", "Chris"));

        new PredicateExercise().whoHaveNoEInYourName(nameCollection);

        assertThat("The result list is not correct", nameCollection.toString(), is("[Patrick, Chris]"));
    }

    @Test
    @Necessity(true)
    public void replaceTheElements() {
        List<String> nameList = new ArrayList<>(Arrays.asList("Joe", "Steven", "Patrick", "Chris"));

        new ReplaceElementExercise().replaceTheElements(nameList);

        assertThat("You do not have replaced correctly the elements", nameList.toString(), is("[(Joe), (Steven), (Patrick), (Chris)]"));
    }

    @Test
    @Necessity(true)
    public void sortByName() {
        List<String> nameList = new ArrayList<>(Arrays.asList("Joe", "Steven", "Patrick", "Chris"));

        new SortExercise().sortByName(nameList);

        assertThat("The list is not sorted correctly", nameList.toString(), is("[Joe, Chris, Steven, Patrick]"));
    }

    @Test
    @Necessity(true)
    public void createStream() {
        Collection<String> nameList = new ArrayList<>(Arrays.asList("Joe", "Steven", "Patrick", "Chris"));

        Stream<String> nameStream = new CreateStreamExercise().createStream(nameList);
        
        assertNotNull("You must return a stream", nameStream);
        assertThat("The stream must have 4 items", nameStream.count(), is(4L));
    }

    @Test
    @Necessity(true)
    public void createParallelStream() {
        Collection<String> nameList = new ArrayList<>(Arrays.asList("Joe", "Steven", "Patrick", "Chris"));

        Stream<String> nameParallelStream = new CreateParallelStreamExercise().createParallelStream(nameList);

        assertNotNull("You must return a stream", nameParallelStream);
        assertThat("The stream must have 4 items", nameParallelStream.count(), is(4L));
        assertTrue("The steam must be parallel", nameParallelStream.isParallel());
    }
}

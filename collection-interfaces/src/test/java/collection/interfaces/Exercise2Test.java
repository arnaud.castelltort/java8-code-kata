package collection.interfaces;

import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import collection.interfaces.exercise2.GetDefaultValueExercise;
import collection.interfaces.exercise2.IgnoringAbsentKeysExercise;
import collection.interfaces.exercise2.MergeValuesExercise;
import collection.interfaces.exercise2.PutIfNotExistingExercise;
import common.test.tool.annotation.Necessity;

public class Exercise2Test {


    private final Map<String, Integer> map = new HashMap<String, Integer>() {{
        put("Joe", 22);
        put("Steven", 27);
        put("Patrick", 28);
        put("Chris", 26);
    }};

    @Test
    @Necessity(true)
    public void getDefaultValue() {
        Map<String, Integer> map = new HashMap<>(this.map);
        Map<String, Integer> map2 = new HashMap<>(this.map);
        map2.put("Alice", 28);

        GetDefaultValueExercise exercise = new GetDefaultValueExercise();
        
        Integer defaultVal = exercise.getDefaultValue(map);
        assertThat("The default value is not correct", defaultVal, is(30));
        
        Integer value = exercise.getDefaultValue(map2);
        assertThat("The value is not returned even if it exists", value, is(28));
    }

    @Test
    @Necessity(true)
    public void putIfNotExisting() {
        Map<String, Integer> map = new HashMap<>(this.map);

        new PutIfNotExistingExercise().putIfNotExisting(map);

        assertThat("Alice was to put in the map", map.get("Alice"), is(32));
        assertThat("Joe was modified while he was already present", map.get("Joe"), is(22));
    }

    @Test
    @Necessity(true)
    public void mergeValues() {
        Map<String, Integer> map = new HashMap<>(this.map);

        new MergeValuesExercise().mergeValues(map);

        assertThat("Alice was not merged correctly", map.get("Alice"), is(32));
        assertThat("Joe was not merged correctly", map.get("Joe"), is(54));
    }

    @Test
    @Necessity(true)
    public void ignoringAbsentKeys() {
        Map<String, Integer> map = new HashMap<>(this.map);

        new IgnoringAbsentKeysExercise().ignoringAbsentKeys(map);

        assertThat("Joe was not incremented correcly", map.get("Joe"), is(23));
        assertThat("Steven was not incremented correcly", map.get("Steven"), is(28));
        assertThat("Alice should not be present in the map", map, not(hasKey("Alice")));
    }
}

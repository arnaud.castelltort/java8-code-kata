# Java 8 Interfaces

## Date


Create a `LocalDate` of 2015-06-18 by using `LocalDate#of`

@[LocalDateOf exercise]({"stubs": ["date/and/time/api/exercice1/LocalDateOfExercise.java"], "command": "date.and.time.api.Exercise1Test#localDateOf"})


Create a `LocalDate` of 2015-06-18 from String by using `LocalDate#parse`

@[LocalDateParse exercice]({"stubs": ["date/and/time/api/exercice1/LocalDateParseExercise.java"], "command": "date.and.time.api.Exercise1Test#localDateParse"})


Create a `LocalDate` from `ld` with year 2016 by using `LocalDate#withYear` or `LocalDate#with`

@[LocalDateWith exercice]({"stubs": ["date/and/time/api/exercice1/LocalDateWithExercise.java"], "command": "date.and.time.api.Exercise1Test#localDateWith"})


Create a `LocalDate` from `ld` adjusted into first day of next year by using `LocalDate#with` and `TemporalAdjusters#firstDayOfNextYear`

@[LocalDateWithAdjuster exercice]({"stubs": ["date/and/time/api/exercice1/LocalDateWithAdjusterExercise.java"], "command": "date.and.time.api.Exercise1Test#localDateWithAdjuster"})


Create a `LocalDate` from `ld` with 10 month laterby using `LocalDate#plusDays` or `LocalDate#plus`

@[LocalDatePlus exercice]({"stubs": ["date/and/time/api/exercice1/LocalDatePlusExercise.java"], "command": "date.and.time.api.Exercise1Test#localDatePlus"})


Create a `LocalDate` from `ld` with 10 days before by using `LocalDate#minusDays` or `LocalDate#minus`

@[LocalDateMinus exercice]({"stubs": ["date/and/time/api/exercice1/LocalDateMinusExercise.java"], "command": "date.and.time.api.Exercise1Test#localDateMinus"})


Define a `Period` of 1 year 2 month 3 days.
Then Create a `LocalDate` adding the period to `ld` by using `LocalDate#plus`

@[LocalDatePlusPeriod exercice]({"stubs": ["date/and/time/api/exercice1/LocalDatePlusPeriodExercise.java"], "command": "date.and.time.api.Exercise1Test#localDatePlusPeriod"})


Check whether `ld2` is after `ld` or not by using `LocalDate#isAfter` or `LocalDate#isBefore`

@[LocalDateIsAfter exercice]({"stubs": ["date/and/time/api/exercice1/LocalDateIsAfterExercise.java"], "command": "date.and.time.api.Exercise1Test#localDateIsAfter"})


Create a period from `ld` till `ld2` by using `LocalDate#until`

@[LocalDateUntil exercice]({"stubs": ["date/and/time/api/exercice1/LocalDateUntilExercise.java"], "command": "date.and.time.api.Exercise1Test#localDateUntil"})

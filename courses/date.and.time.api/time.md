# Java 8 Interfaces

## Time


Create a `LocalTime` of 23:07 by using `LocalTime#of`

@[LocalTimeOfHourToMinute exercice]({"stubs": ["date/and/time/api/exercice2/LocalTimeOfHourToMinuteExercise.java"], "command": "date.and.time.api.Exercise2Test#localTimeOfHourToMinute"})


Create a `LocalTime` of 23:07:03.1 by using `LocalTime#of`

@[LocalTimeOfHourToNanoSec exercice]({"stubs": ["date/and/time/api/exercice2/LocalTimeOfHourToNanoSecExercise.java"], "command": "date.and.time.api.Exercise2Test#localTimeOfHourToNanoSec"})


Create a `LocalTime` of 23:07:03.1 from String by using `LocalTime#parse`

@[LocalTimeParse exercice]({"stubs": ["date/and/time/api/exercice2/LocalTimeParseExercise.java"], "command": "date.and.time.api.Exercise2Test#localTimeParse"})


Create a `LocalTime` from `lt` with hour 21 by using `LocalTime#withHour` or `LocalTime#with`

@[LocalTimeWith exercice]({"stubs": ["date/and/time/api/exercice2/LocalTimeWithExercise.java"], "command": "date.and.time.api.Exercise2Test#localTimeWith"})


Create a `LocalTime` from `lt` with 30 minutes later by using `LocalTime#plusMinutes` or `LocalTime#plus`
     
@[LocalTimePlus exercice]({"stubs": ["date/and/time/api/exercice2/LocalTimePlusExercise.java"], "command": "date.and.time.api.Exercise2Test#localTimePlus"})


Create a `LocalTime` from `lt` with 3 hours before by using `LocalTime#minusHours` or `LocalTime#minus`
          
@[LocalTimeMinus exercice]({"stubs": ["date/and/time/api/exercice2/LocalTimeMinusExercise.java"], "command": "date.and.time.api.Exercise2Test#localTimeMinus"})


Define a `Duration` of 3 hours 30 minutes and 20.2 seconds.
Then create a `LocalTime` subtracting the duration from `lt` by using `LocalTime#minus`
          
@[LocalTimeMinusDuration exercice]({"stubs": ["date/and/time/api/exercice2/LocalTimeMinusDurationExercise.java"], "command": "date.and.time.api.Exercise2Test#localTimeMinusDuration"})


Check whether `lt2` is before `lt` or not by using `LocalTime#isAfter` or `LocalTime#isBefore`
          
@[LocalTimeIsBefore exercice]({"stubs": ["date/and/time/api/exercice2/LocalTimeIsBeforeExercise.java"], "command": "date.and.time.api.Exercise2Test#localDateIsBefore"})


Create a `LocalTime` from `lt` truncated to minutes by using `LocalTime#truncatedTo`
          
@[LocalTimeTruncatedTo exercice]({"stubs": ["date/and/time/api/exercice2/LocalTimeTruncatedToExercise.java"], "command": "date.and.time.api.Exercise2Test#localTimeTruncatedTo"})

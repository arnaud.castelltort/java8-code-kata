# Java8 Code Kata

## What is Code Kata?

According to [www.codekatas.org](http://www.codekatas.org/)
> Code Kata is a term coined by Dave Thomas, co-author of the book The Pragmatic Programmer, in a bow to the Japanese concept of kata in the martial arts. A code kata is an exercise in programming which helps a programmer hone their skills through practice and repetition.


Also refer [codekata.com](http://codekata.com/)

## What is Java8 Code Kata?

The _Java8 Code Kata_ is created to walk-through java8 new API functions. I hope this helps you learn Java8 and get used to it. Repeating exercises will definitely upgrade your skills.



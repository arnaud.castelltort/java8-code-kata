# Java 8 Interfaces

## Collections

`forEach()` method in the List has been inherited from `java.lang.Iterable` and `removeIf()` method has been inherited
from `java.util.Collection`. `replaceAll()` and `sort()` methods are from `java.util.List`.
All these methods have been added in Java 8.

Here is an exercise about `forEach()` method using a `Consumer`

@[ForEach exercice]({"stubs": ["collection/interfaces/exercise1/ForEachExercise.java", "common/test/tool/entity/Customer.java"], "command": "collection.interfaces.Exercise1Test#iterateByForEach"})

Here is an exercise about `removeIf()` method using a `Predicate`
Remove elements from the list which contains "e" using `removeIf()`

@[Predicate exercice]({"stubs": ["collection/interfaces/exercise1/PredicateExercise.java"], "command": "collection.interfaces.Exercise1Test#whoHaveNoEInYourName"})

Now try to replace all `String` of the list with as `UnaryOperator`.
Replace the elements in `nameList` with string wrapped with "()" using `replaceAll`

@[Element replacement exercice]({"stubs": ["collection/interfaces/exercise1/ReplaceElementExercise.java"], "command": "collection.interfaces.Exercise1Test#replaceTheElements"})

Create a `Comparator` to sort the name list by their name's length in ascending order.

@[Sort exercice]({"stubs": ["collection/interfaces/exercise1/SortExercise.java"], "command": "collection.interfaces.Exercise1Test#sortByName"})

Create a serial `Stream` using `Collection#stream`
You can learn about `Stream` APIs at stream-api module.

@[Stream exercice]({"stubs": ["collection/interfaces/exercise1/CreateStreamExercise.java"], "command": "collection.interfaces.Exercise1Test#createStream"})

Create a parallel `Stream` using `Collection#parallelStream` or `Stream#parallel`

@[Parallel stream exercice]({"stubs": ["collection/interfaces/exercise1/CreateParallelStreamExercise.java"], "command": "collection.interfaces.Exercise1Test#createParallelStream"})

CAAAAAAATTTTTTTTTTTTTTT
![cat](courses/images/cat.png)


![cat](courses/images/cat.png)

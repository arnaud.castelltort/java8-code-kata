# Java 8 Interfaces

## Hashmap


Try to get from key "Alice" using `Map#getOrDefault`. If the key doesn't exist, use 30 as default.

@[GetOrDefault exercice]({"stubs": ["collection/interfaces/exercise2/GetDefaultValueExercise.java"], "command": "collection.interfaces.Exercise2Test#getDefaultValue"})


Try to put 2 entry with key as "Alice" value as 32, key as "Joe" and value as 32 using `Map#putIfAbsent`.

@[PutIfNotExisting exercice]({"stubs": ["collection/interfaces/exercise2/PutIfNotExistingExercise.java"], "command": "collection.interfaces.Exercise2Test#putIfNotExisting"})


Merge 2 entry to `map` with key="Alice" value=32, key="Joe" value=32 using `Map#merge`.
If the value already exist for the key, remap with sum value.

@[MergeValues exercice]({"stubs": ["collection/interfaces/exercise2/MergeValuesExercise.java"], "command": "collection.interfaces.Exercise2Test#mergeValues"})


Try to increment the value for keys "Joe", "Steven" and "Alice" using `Map#computeIfPresent`.

@[IgnoringAbsentKeys exercice]({"stubs": ["collection/interfaces/exercise2/IgnoringAbsentKeysExercise.java"], "command": "collection.interfaces.Exercise2Test#ignoringAbsentKeys"})
